package com.dani.ioasys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class EmpresasJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpresasJavaApplication.class, args);
	}

}
