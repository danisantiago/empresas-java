package com.dani.ioasys.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dani.ioasys.model.Enterprise;
import com.dani.ioasys.service.EnterpriseService;

@RestController
@RequestMapping(value = "api/{api_version}/enterprises")
public class EnterpriseController {

	@Autowired
	private EnterpriseService service;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Enterprise> findById(@PathVariable Integer id) {
		Enterprise obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}

	@GetMapping()
	public ResponseEntity<List<Enterprise>> search(
			@RequestParam(value = "nome", defaultValue = "") String name,
			@RequestParam(value = "enterprise_types", defaultValue = "-1") Integer id) {

		List<Enterprise> list = new ArrayList<Enterprise>();
		if(name.isEmpty() && id == -1) {
			list = service.findAllService();
		}else if(name.isEmpty() && id != -1) {
			list = service.findByType(id);
		}else if(!name.isEmpty() && id == -1){
			list = service.findByName(name);
		}else {
			list = service.findByNameAndType(name, id);
		}
		return ResponseEntity.ok().body(list);
	}

}
