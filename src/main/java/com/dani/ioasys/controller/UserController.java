package com.dani.ioasys.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dani.ioasys.dto.CredentialsDTO;
import com.dani.ioasys.model.Investor;
import com.dani.ioasys.model.UserResponse;
import com.dani.ioasys.service.UserDetailsServiceImpl;

@RestController
@RequestMapping(value ="api/{api_version}/users")
public class UserController {

	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@PostMapping(value = "/auth/sign_in")
	public ResponseEntity<?> singIn(@Validated @RequestBody CredentialsDTO body){
		
		Investor investor = userDetailsServiceImpl.findByEmail(body.getEmail());
		UserResponse response = new UserResponse();
		response.setInvestor(investor);
		response.setSuccess(true);
		
		if(investor.getPortfolio().getEnterprisesNumber() > 0) {
			response.setEnterprise(null);
		}else {
			String enterprise = investor.getPortfolio().getEnterprises().get(0);
			response.setEnterprise(enterprise);
		}
		
		return ResponseEntity.ok().body(response);
	}

}
