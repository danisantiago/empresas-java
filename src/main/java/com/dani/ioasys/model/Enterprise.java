package com.dani.ioasys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
public class Enterprise implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String enterprise_name;
    private String description;
    private String email_enterprise;
    private String facebook;
    private String twitter;
    private String linkedin;
    private String phone;
    private boolean own_enterprise;
    private String photo;
    private int value;
    private int shares;
    private double share_price;
    private int own_shares;
    private String city;
    private String country;
    
    @ManyToOne
    @SerializedName(value = "enterprise_type")
    private EnterpriseType enterpriseTypes;
    
}