package com.dani.ioasys.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
public class EnterpriseType {
	
	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
    private int id;
    private String enterprise_type_name;
}
