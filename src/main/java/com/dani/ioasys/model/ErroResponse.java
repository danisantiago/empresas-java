package com.dani.ioasys.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ErroResponse {

	private boolean success;
	private List<String> erros;
	
}
