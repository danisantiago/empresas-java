package com.dani.ioasys.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@Entity
public class Investor implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Integer id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private double balance;
    private byte[] photo;
    
    @OneToOne
    private Portfolio portfolio;
    private double portfolio_value;
    private boolean first_access;
    private boolean super_angel;
    
    @JsonIgnore
    private String password;

}
