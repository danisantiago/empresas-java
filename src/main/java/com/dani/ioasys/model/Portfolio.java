package com.dani.ioasys.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter
@NoArgsConstructor
public class Portfolio {

	@Id @GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	private int enterprisesNumber;
	private List<String> enterprises;
}
