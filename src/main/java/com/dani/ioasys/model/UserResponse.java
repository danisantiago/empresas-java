package com.dani.ioasys.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class UserResponse {

	private Investor investor;
	private String enterprise;
	private boolean success;
}
