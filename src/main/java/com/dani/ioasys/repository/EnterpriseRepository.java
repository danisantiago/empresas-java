package com.dani.ioasys.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dani.ioasys.model.Enterprise;


@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, Integer> {
	List<Enterprise> findByEnterpriseName(String enterprise_name);
	List<Enterprise> findByEnterpriseTypes_id(Integer id);
	List<Enterprise> findByEnterpriseNameAndEnterpriseTypes_id(String enterprise_name, Integer id);
	
}
