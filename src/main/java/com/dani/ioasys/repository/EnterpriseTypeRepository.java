package com.dani.ioasys.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dani.ioasys.model.EnterpriseType;

@Repository
public interface EnterpriseTypeRepository extends JpaRepository<EnterpriseType, Integer> {

}
