package com.dani.ioasys.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dani.ioasys.model.Investor;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, Integer> {
	Optional<Investor> findByEmail(String email);
}