package com.dani.ioasys.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dani.ioasys.model.Enterprise;
import com.dani.ioasys.repository.EnterpriseRepository;
import com.dani.ioasys.service.exception.ObjectNotFoundException;

@Service
public class EnterpriseService {

	@Autowired
	private EnterpriseRepository repo;

	public List<Enterprise> findAllService() {
		return repo.findAll();
	}
	
	public Enterprise find(Integer id) {
		Optional<Enterprise> enterprise = repo.findById(id);
		return enterprise.orElseThrow(() -> new ObjectNotFoundException("Not found! Id: " + id +
				", Tipo: " + Enterprise.class));
	}
	
	public List<Enterprise> findByName(String name){
		List<Enterprise> list = repo.findByEnterpriseName(name);
	    return list;
	}
	
	public List<Enterprise> findByType(Integer id){
		List<Enterprise> list = repo.findByEnterpriseTypes_id(id);
		return list;
	}
	
	public List<Enterprise> findByNameAndType(String name, Integer id){
		List<Enterprise> list = repo.findByEnterpriseNameAndEnterpriseTypes_id(name, id);
		return list;
	}

}
