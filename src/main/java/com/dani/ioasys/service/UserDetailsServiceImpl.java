package com.dani.ioasys.service;



import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dani.ioasys.model.Investor;
import com.dani.ioasys.repository.InvestorRepository;
import com.dani.ioasys.security.UserSS;


@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private InvestorRepository repo;
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Investor investor = findByEmail(email);
		return new UserSS(investor.getId(), investor.getEmail(), investor.getPassword(), null);
	}
	
	public Investor findByEmail(String email) {
		Optional<Investor> investor = repo.findByEmail(email);
		if(investor.isPresent()) {
			return investor.get();
		}else {
			throw new UsernameNotFoundException(email);
		}
	}
}